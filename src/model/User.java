package model;

import java.util.Date;
import java.util.List;


public class User {
	private String userName;
	private String passWord;
	private boolean sex;
	private String reSex;
	private Date birthDate;
	private String origin;
	private List<String> languages;
	
	public User() { }
	
	public User(String userName, String passWord, boolean sex, Date birthDate, String origin, List<String> languages) {
		this.userName = userName;
		this.passWord = passWord;
		this.sex = sex;
		this.birthDate = birthDate;
		this.origin = origin;
		this.languages = languages;
	}
	
	public User(String userName, String passWord) {
		super();
		this.userName = userName;
		this.passWord = passWord;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public boolean isSex() {
		return sex;
	}

	public void setSex(boolean sex) {
		this.sex = sex;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public List<String> getLanguages() {
		return languages;
	}

	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getReSex() {
		if(sex == true){
			reSex = "Male";
		}else{
			reSex ="Female";
		}
		return reSex;
	}

	public void setReSex(String reSex) {
		this.reSex = reSex;
	}
	
}
