package model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class UserImpl{
	private static List<User> listUser = new ArrayList<User>();
	
	public UserImpl() {
		Init();
		getOrigin();
	}
	
	public void Init(){
		if(listUser == null || listUser.size() == 0){
			listUser.add(new User("Long","1234",true,fomartStringtoDay("22-1-2015"),"VietNam",addListLanguages()));
			listUser.add(new User("Ha","12345",false,fomartStringtoDay("21-1-1994"),"China",addListLanguages()));
			listUser.add(new User("Ngoc","123456",true,fomartStringtoDay("21-1-1994"),"British",addListLanguages()));
			listUser.add(new User("Binh","1234567",false,fomartStringtoDay("21-1-1994"),"Cambodia",addListLanguages()));
		}
	}
	
	public static List<String> addListLanguages(){
		return Arrays.asList("VietNammes","Japanes","English");
	}
	
	public static List<String> getOrigin(){
		return Arrays.asList("VietNam","China","British","Cambodia");
	}
	
	private Date fomartStringtoDay(String day){
		DateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
		Date date = null;
		try {
			date = formatter.parse(day);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	
	public static boolean searchUser(String userName, String passWord){
		if(userName == null || passWord == null || "".equals(userName) || "".equals(passWord)) {
			return false;
		} else {
			for(User u : listUser) {
				if(userName.equals(u.getUserName()) && passWord.equals(u.getPassWord()))
					return true;
			}
		}
		return false;
	}
	
	public List<User> selectAll(){
		return listUser;
	}
}
