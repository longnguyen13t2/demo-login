package viewmodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.User;
import model.UserImpl;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

public class UserController{
	
	private User user = new User();
	UserImpl uimp = new UserImpl();
	private List<User> listUser;
	private ListModel<String> originModel = new ListModelList<String>(UserImpl.getOrigin());
	
	
	
	public ListModel<String> getOriginModel() {
		return originModel;
	}

	public void setOriginModel(ListModel<String> originModel) {
		this.originModel = originModel;
	}

	public void changeOrigin() {
        
    }
	
	@Command
	public void updateInfo(@BindingParam("newUser") User user, @BindingParam("win") Window win){
		System.out.println(user.getUserName());
		win.detach();
	}
	
	@Command
	public void deleteUser(){
		listUser.remove(user);
		Executions.sendRedirect("/listUser.zul");
	}
	
	@Command
	public void changeInfo(){
		if(user.getUserName() != null){
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("user", user);
			Executions.createComponents("/updateUser.zul", null, map);
		}	
	}
	
	@Command
	public void inputUser(){
		uimp.Init();
		listUser.add(user);
		Executions.sendRedirect("/listUser.zul");
	}
	
	@Command
	public void changeSubmitStatus() {
		boolean a = UserImpl.searchUser(user.getUserName(), user.getPassWord());
		if(a == true){
			Executions.sendRedirect("/inputUser.zul");
		}
	}
	
	@Init
	public void selectAllUser() {
		listUser = uimp.selectAll();
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getListUser() {
		return listUser;
	}

	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}
	
}
